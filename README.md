# Parallel pipelines

This project exemplifies how to trigger a set of parallel pipelines; making their execution completely independent from each other. 

This is usefull when you need to perform actions in parallel (such as deployments) without using acyclic graphs https://docs.gitlab.com/ee/ci/directed_acyclic_graph/